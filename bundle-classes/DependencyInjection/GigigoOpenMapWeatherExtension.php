<?php
namespace Gigigo\OpenMapWeatherBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class GigigoOpenMapWeatherExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->loadServices($container);
        $this->loadConfig($configs, $container);
    }

    private function loadServices(ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    private function loadConfig(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('gigigo_open_map_weather.api.api_url',
            $config['api']['api_url']
        );

        $container->setParameter('gigigo_open_map_weather.api.api_key',
            $config['api']['api_key']
        );

        $container->setParameter('gigigo_open_map_weather.api.units',
            $config['api']['units']
        );
        
        $container->setParameter('gigigo_open_map_weather.api.lang',
            $config['api']['lang']
        );
    }
}