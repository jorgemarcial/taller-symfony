<?php

/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Gigigo\OpenMapWeatherBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GigigoOpenMapWeatherBundle extends Bundle
{
}
