# Desarrollo de librerias / bundles #

## Dada la siguiente [clase](https://bitbucket.org/jorgemarcial/taller-symfony/raw/99846a259e2e7a1949ad28a618a96c9a31a7e8b5/openweathermap.class.php) crear un bundle para Symfony.

* [Instalar esqueleto con Symfony](https://symfony.com/download)
* Crear repositorio en bitbucket/github con nombre openmapweatherbundle. Nos quedara resultante algo parecido a {usuario}/openmapweatherbundle
* Clonar repositorio en nuestro workspace (fuera del bundle).
* Añadir en dicho repositorio un fichero composer.json, con la siguiente estructura:
```json
{
    "name": "{user}/openmapweatherbundle",
    "description": "OpenMapWeatherBundle",
    "type": "library",
    "license": "MIT",
    "authors": [
        {
            "name": "Gigigo",
            "email":"gigigo@gigigo.com"
        }
    ],
    "autoload": {
        "psr-4": { "Gigigo\\OpenMapWeatherBundle\\": "" }
    },
    "require": {
        "php": ">=5.3.0",
        "guzzlehttp/guzzle": "^5.0|^6.0"
    }
}
```
* Commit en rama master y push.
* Ir al directorio del proyecto, modificar composer.json añadiendo lo siguiente (recordar que si el repositorio está alojado en satis sólo necesitariamos añadir la parte del require):

```json
    "repositories": [
        {
            "type": "vcs",
            "url": "{URL REPOSITORIO GITHUB/BITBUCKET}"
        }
    ],
    "require": {
        ...
        "{user}/openmapweatherbundle": "dev-master"
    },
```
* Salvar y ejecutar:
```bash
composer require {user}/openmapweatherbundle dev-master
```


### Creamos el bundle.

* Bajamos hasta el directorio vendor/{user}/openmapweatherbundle
* Añadimos carpeta DependencyInjection. Codigo [aquí](https://bitbucket.org/jorgemarcial/taller-symfony/src/99846a259e2e7a1949ad28a618a96c9a31a7e8b5/bundle-classes/DependencyInjection/?at=master).
* Añadimos carpeta Resources/config. Código [aquí](https://bitbucket.org/jorgemarcial/taller-symfony/src/c01fb41643a2/bundle-classes/Resources/config/?at=master)
* Añadimos carpeta Services. Código [aquí](https://bitbucket.org/jorgemarcial/taller-symfony/src/99846a259e2e/bundle-classes/Services/?at=master)
* Añadimos clase del bundle. Código [aquí](https://bitbucket.org/jorgemarcial/taller-symfony/src/cf506f8b7d0e45e5a3f13b37d42ea2aeb3d4c417/bundle-classes/GigigoOpenMapWeatherBundle.php?at=master&fileviewer=file-view-default)


### Configuración del proyecto

* Añádir bundle en AppKernel

```php
new Gigigo\OpenMapWeatherBundle\GigigoOpenMapWeatherBundle()
```

Añadimos en config.yml:

```yml
gigigo_open_map_weather:
    api:
        api_url: 'http://api.openweathermap.org/data/2.5/'
        api_key: 'f73b2ad72f1300dcc8991183979fa5e5'
        units: 'metric'
        lang: 'es'
```

*Añadimos a la clase DefaultController los siguientes métodos:
```php

use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{

    ....

    /**
     * @Route("/weather", name="wheater")
     */
    public function weatherAction(Request $request)
    {
        $openMapWeatherApi = $this->container->get('gigigo.openmapweather.api');
        $response = new JsonResponse();
        $data = $openMapWeatherApi->getWeather('Madrid');
        $response->setData(
            $data
        );

        return $response;
    }

    /**
     * @Route("/forecast", name="forecast")
     */
    public function forecastAction(Request $request)
    {
        $openMapWeatherApi = $this->container->get('gigigo.openmapweather.api');
        $response = new JsonResponse();
        $data = $openMapWeatherApi->getForecast('Madrid');
        $response->setData(
            $data
        );

        return $response;
    }
}

```

* Levantar proyecto con servidor embebido:

```bash
./bin/console ser:run localhost:8000
```

* Obtendremos una respuesta parecida a la siguiente:

```json
{"coord":{"lon":-3.7,"lat":40.42},"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"base":"stations","main":{"temp":280.15,"pressure":1003,"humidity":87,"temp_min":280.15,"temp_max":280.15},"visibility":10000,"wind":{"speed":2.6,"deg":340},"clouds":{"all":75},"dt":1479891600,"sys":{"type":1,"id":5488,"message":0.2063,"country":"ES","sunrise":1479885050,"sunset":1479919902},"id":3117735,"name":"Madrid","cod":200}
```

* Volvemos a vendor/{user}/openmapwatherbundle
* Commit y push a dev-master

* Volvemos a la raíz del proyecto.
* Actualizamos composer.lock, mediante:

```bash
composer require {user}/openmapweatherbundle dev-master
```